---
title: " - Software Libre"
date: 2018-11-14T12:17:12-04:00
draft: false
anchor: "software_libre"
weight: 3
---

El «Software Libre» es un asunto de libertad, no de precio. Para entender el concepto, debe pensarse en «libre» como en «libertad de expresión», no como en «cerveza gratis».

El software libre es una cuestión de libertad de los usuarios para ejecutar, copiar, distribuir, estudiar, cambiar y modificar el software. Más concretamente se refiere a los cuatro tipos de libertades para los usuarios de software:

La libertad de usar el programa, con cualquier propósito (libertad 0).
La libertad de estudiar el funcionamiento del programa, y adaptarlo a las necesidades (libertad 1). El acceso al código fuente es una condición previa para esto.
La libertad de distribuir copias, con lo que puede ayudar a otros (libertad 2).
La libertad de mejorar el programa y hacer públicas las mejoras, de modo que toda la comunidad se beneficie (libertad 3). De igual forma que la libertad 1 el acceso al código fuente es un requisito previo.

http://www.gnu.org/philosophy/free-sw.es.html
