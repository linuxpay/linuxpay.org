# [Linuxpay.org](https://linuxpay.org/) 
Éste es el sitio del Grupo de Usuarios de Linux de Paysandú - Uruguay.

Está creado usando el generador de sitios estáticos [Hugo](https://gohugo.io/) y el tema [Kraiklyn](https://themes.gohugo.io/kraiklyn/).

Está hosteado en [Netlify](https://www.netlify.com/): [![Netlify Status](https://api.netlify.com/api/v1/badges/d679c165-eccc-4f0b-ba2b-3a12aaccdd4a/deploy-status)](https://app.netlify.com/sites/linuxpay-web/deploys)

Sientanse libre de modificar lo que crean necesario y hacer un pull request.

# Insturcciones:

Estas instrucciones son suponiendo que tienen alguna distribución de linux/bsd/unix. Como dependencias también tienen que tener installado git y hugo.

## Levantar el sitio en su PC:

1. Clonar el reposiotrio

```
git clone git@gitlab.com:emitor/linuxpay.org.git
cd ./linuxpay.org
# Clonar el theme
git submodule init
git submodule update
```

2. Ejecutar hugo
```
hugo server
```

3. Ir al navegador web y acceder a la url: http://localhost:1313/

Con eso ya tiene el sitio funcionando localmente, pueden hacer todos los cambios que quieran y los ven localmente sin modificar el sitio en internet. Una vez que están contentos pueden hacer un pull request.

Por más info en como usar hugo pueden chequear la documentación https://gohugo.io/getting-started/